package edu.wm.cs.cs301.PhillipWatkins.generation;

import android.app.Application;
import android.util.Log;

import edu.wm.cs.cs301.PhillipWatkins.gui.AMazeActivity;

/**
 * Provides access to the Maze anywhere in the App.
 *
 */
public class AMazeGlobalContainer extends Application {

    private static final String TAG = AMazeActivity.class.getName(); // Tag to tell where message came from

    private Maze globalMaze;

    /**
     * Returns the Maze or MazeConfiguration.
     * This should only be called by Play...Activity.java classes, and
     * should be destroyed when it is no longer needed (when the game is over).
     * @return
     */
    public Maze getGlobalMaze() {
        Log.v(TAG, "Globally accessible Maze was retreived with getGlobalMaze() method.");
        return globalMaze;
    }

    /**
     * Sets the globalMaze for Application-wide access.
     * @param globalMaze
     */
    public void setGlobalMaze(Maze globalMaze) {
        Log.v(TAG, "Globally accessible Maze was set with setGlobalMaze() method.");
        this.globalMaze = globalMaze;
    }

    /**
     * Destroys the Maze configuration. This should always be called
     * when the game ends to prevent memory leaks. Maze is garbage
     * collected after this is called and will no longer be accessible.
     */
    public void destroyMaze() {
        Log.v(TAG, "Globally accessible Maze was set to null with destroyMaze() method.");
        globalMaze = null;
    }
}
