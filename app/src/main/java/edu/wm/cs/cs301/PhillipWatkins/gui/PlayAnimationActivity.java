package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.PhillipWatkins.R;
import edu.wm.cs.cs301.PhillipWatkins.generation.AMazeGlobalContainer;
import edu.wm.cs.cs301.PhillipWatkins.generation.CardinalDirection;
import edu.wm.cs.cs301.PhillipWatkins.generation.Floorplan;
import edu.wm.cs.cs301.PhillipWatkins.generation.Maze;

/**
 * Responsible for showing the robot driving itself through the maze,
 * and then proceeding to the Win/Lose screen as necessary.
 * Also responsible for setting the Global Maze Variable
 * to null so that is can be Garbage Collected.
 * @author Phillip Watkins
 */
public class PlayAnimationActivity extends AppCompatActivity {

    private static final String TAG = "PlayAnimationActivity"; // Tag to tell where message came from

    private ProgressBar batteryProgress;
    private Handler progBarHandler = new Handler();
    private Switch toggleMap;
    private Switch toggleShowWalls;
    private Switch toggleShowSolution;
    private TextView batteryVal;

    private ToggleButton startOrPauseDriver;
    private int mapZoomLevel = 75;
    private Button zoomIn;
    private Button zoomOut;
    private Button leftSensor;
    private Button rightSensor;
    private Button frontSensor;
    private Button backSensor;
    Thread frontSensorThread;
    Thread rightSensorThread;
    Thread backSensorThread;
    Thread leftSensorThread;
    private Boolean frontIsInRepairCycle = false;
    private Boolean backIsInRepairCycle = false;
    private Boolean leftIsInRepairCycle = false;
    private Boolean rightIsInRepairCycle = false;
    private Boolean interruptedThreads;
    private TextView displayZoomValue;
    MazePanel panel;
    Maze maze;
    BasicRobot robot;
    RobotDriver driver;
    Thread batteryThread;
    Thread driverThread;
    boolean wonGame; // true if they reached the exit, false otherwise
    boolean startedDriver;
    boolean paused = true; // paused until player clicks start
    private Intent shareGamedata;

    FirstPersonView firstPersonView;
    Map mapView;
    boolean showMaze, showSolution, mapMode;
    int px, py ; // current position on maze grid (x,y)
    int dx, dy;  // current direction

    int angle; // current viewing angle, east == 0 degrees
    int walkStep; // counter for intermediate steps within a single step forward or backward
    Floorplan seenCells;
    int shortestPathLength;
    private static int delta_t = 3000; // constant for delaying sensor failure/reapir -- 3 seconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);


        toggleMap = findViewById(R.id.mapToggleAnim);
        toggleShowWalls = findViewById(R.id.showWallsToggleAnim);
        toggleShowSolution = findViewById(R.id.solutionToggleAnim);
        startOrPauseDriver = findViewById(R.id.toggleDriver);
        zoomIn = findViewById(R.id.mapZoomIn);
        zoomOut = findViewById(R.id.mapZoomOut);
        displayZoomValue = findViewById(R.id.mapZoomValue);
        displayZoomValue.setText("Map zoom level: 75"); // Default zoom is 15/30
        leftSensor = findViewById(R.id.leftSensor);
        frontSensor = findViewById(R.id.frontSensor);
        backSensor = findViewById(R.id.backSensor);
        rightSensor = findViewById(R.id.rightSensor);
        batteryVal = findViewById(R.id.batteryValue);

        panel = findViewById(R.id.playingView);

        // Get the maze
        maze = ((AMazeGlobalContainer) getApplication()).getGlobalMaze();

        showMaze = true ;
        showSolution = true ;
        mapMode = true ;
        startedDriver = false;
        interruptedThreads = false;
        // init data structure for visible walls
        seenCells = new Floorplan(maze.getWidth()+1,maze.getHeight()+1) ;
        // set the current position and direction consistently with the viewing direction
        setPositionDirectionViewingDirection();
        walkStep = 0; // counts incremental steps during move/rotate operation
        if (panel != null) {
            startDrawer();
        }

        initializeRobotAndDriver();

        batteryProgress = findViewById(R.id.batteryProgressBar);
        batteryProgress.setBackgroundColor(0x00FFFFFF); // makes background transparent instead of white

        setUpMapToggle();
        setUpShowSolutionToggle();
        setUpShowWallsToggle();


        setUpDriverToggleButton();
        setUpZoomButtons();

        setUpBackSensorButton();
        setUpFrontSensorButton();
        setUpLeftSensorButton();
        setUpRightSensorButton();

    }

    /**
     * Returns to title screen activity instead of previous screen
     */
    @Override
    public void onBackPressed() {
        interruptedThreads = true;
        super.onBackPressed();
        if (driverThread != null) {
            if (driverThread.isInterrupted() == false)
                driverThread.interrupt();
            driverThread = null;
        }
        backIsInRepairCycle = frontIsInRepairCycle = leftIsInRepairCycle = rightIsInRepairCycle = false;
        ((AMazeGlobalContainer)getApplication()).destroyMaze();
        startActivity(new Intent(this, AMazeActivity.class));
        finish();
    }

    /**
     * Adds onCheckedChange listener to map toggle
     */
    private void setUpMapToggle() {
        toggleMap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v(TAG, "Map toggled");
                mapMode = !mapMode;
                draw();
            }
        });
    }

    /**
     * Adds listener to show solution toggle switch
     */
    private void setUpShowSolutionToggle() {
        toggleShowSolution.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v(TAG, "Toggled show solution");
                showSolution = !showSolution;
                draw();
            }
        });
    }

    /**
     * Adds listener to show walls toggle switch
     */
    private void setUpShowWallsToggle() {
        toggleShowWalls.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v(TAG, "Toggled show walls");
                showMaze = !showMaze;
                draw();
            }
        });
    }

    /**
     * Sets up button to start/pause driver int its own thread
     */
    private void setUpDriverToggleButton() {
        startOrPauseDriver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                paused = !paused;
                if (!startedDriver) {
                    driverThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            startedDriver = true;
                            Log.v(TAG, "Started driver");
                            try {
                                wonGame = driver.drive2Exit();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

//                            if (!interruptedThreads) // need his condition?
                                giveBatteryAndPathInformation();
                        }
                    });
                    driverThread.start();
                }
            }
        });
    }

    /**
     * Sets up zoom in/out buttons and adjust the textview to display the zoom level (0-30)
     * 0 is smallest map view, 30 is largest.
     */
    private void setUpZoomButtons() {
        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapZoomLevel++;
                Log.v(TAG, "Map zoomed in");
                displayZoomValue.setText("Map zoom level: " + (mapZoomLevel));
                adjustMapScale(true);
                draw();
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapZoomLevel--;
                if (mapZoomLevel < 0)
                    mapZoomLevel = 0;
                Log.v(TAG, "Map zoomed out");
                displayZoomValue.setText("Map zoom level: " + (mapZoomLevel));
                adjustMapScale(false);
                draw();
            }
        });
    }

    /**
     * Sets up Back sensor button and triggers sensor failures and repairs every 3 seconds when clicked.
     * Clicking the button again will stop the failure/repair cycle.
     */
    private void setUpBackSensorButton() {
        backSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backIsInRepairCycle = !backIsInRepairCycle;
                backSensorThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (true) {
                                if (backIsInRepairCycle) {
                                    Log.v(TAG, "Started a back sensor failure/repair cycle");
                                    backSensorThread.sleep(delta_t);
                                    backSensor.setBackgroundColor(Color.RED);
                                    robot.triggerSensorFailure(Robot.Direction.BACKWARD);
                                    backSensorThread.sleep(delta_t);
                                    robot.repairFailedSensor(Robot.Direction.BACKWARD);
                                    backSensor.setBackgroundColor(Color.GREEN);
                                    driver.triggerUpdateSensorInformation();
                                } else {
                                    Log.v(TAG, "Back sensor failure/repair stopped by button press.");
                                    backSensor.setBackgroundColor(Color.LTGRAY);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (backIsInRepairCycle) {
                    backSensorThread.start();
                    backSensor.setBackgroundColor(Color.GREEN);
                } else {
                    backSensorThread.interrupt();
                }
            }
        });
    }

    /**
     * Sets up Front sensor button and triggers sensor failures and repairs every 3 seconds when clicked.
     * Clicking the button again will stop the failure/repair cycle.
     */
    private void setUpFrontSensorButton() {
        frontSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frontIsInRepairCycle = !frontIsInRepairCycle;
                frontSensorThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (true) {
                                if (frontIsInRepairCycle) {
                                    Log.v(TAG, "Started a front sensor failure/repair cycle");
                                    frontSensorThread.sleep(delta_t);
                                    frontSensor.setBackgroundColor(Color.RED);
                                    robot.triggerSensorFailure(Robot.Direction.FORWARD);
                                    frontSensorThread.sleep(delta_t);
                                    robot.repairFailedSensor(Robot.Direction.FORWARD);
                                    frontSensor.setBackgroundColor(Color.GREEN);
                                    driver.triggerUpdateSensorInformation();
                                } else {
                                    Log.v(TAG, "Front sensor failure/repair stopped by button press.");
                                    frontSensor.setBackgroundColor(Color.LTGRAY);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (frontIsInRepairCycle) {
                    frontSensorThread.start();
                    frontSensor.setBackgroundColor(Color.GREEN);
                } else {
                    frontSensorThread.interrupt();
                }
            }
        });
    }

    /**
     * Sets up Left sensor button and triggers sensor failures and repairs every 3 seconds when clicked.
     * Clicking the button again will stop the failure/repair cycle.
     */
    private void setUpLeftSensorButton() {
        leftSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leftIsInRepairCycle = !leftIsInRepairCycle;
                leftSensorThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (true) {
                                if (leftIsInRepairCycle) {
                                    Log.v(TAG, "Started a left sensor failure/repair cycle");
                                    leftSensorThread.sleep(delta_t);
                                    leftSensor.setBackgroundColor(Color.RED);
                                    robot.triggerSensorFailure(Robot.Direction.LEFT);
                                    leftSensorThread.sleep(delta_t);
                                    robot.repairFailedSensor(Robot.Direction.LEFT);
                                    leftSensor.setBackgroundColor(Color.GREEN);
                                    driver.triggerUpdateSensorInformation();
                                } else {
                                    Log.v(TAG, "Left sensor failure/repair stopped by button press.");
                                    leftSensor.setBackgroundColor(Color.LTGRAY);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (leftIsInRepairCycle) {
                    leftSensorThread.start();
                    leftSensor.setBackgroundColor(Color.GREEN);
                } else {
                    leftSensorThread.interrupt();
                }
            }
        });
    }

    /**
     * Sets up Right sensor button and triggers sensor failures and repairs every 3 seconds when clicked.
     * Clicking the button again will stop the failure/repair cycle.
     */
    private void setUpRightSensorButton() {
        rightSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightIsInRepairCycle = !rightIsInRepairCycle;
                rightSensorThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            while (true) {
                                if (rightIsInRepairCycle) {
                                    Log.v(TAG, "Started a right sensor failure/repair cycle");
                                    rightSensorThread.sleep(delta_t);
                                    rightSensor.setBackgroundColor(Color.RED);
                                    robot.triggerSensorFailure(Robot.Direction.RIGHT);
                                    rightSensorThread.sleep(delta_t);
                                    robot.repairFailedSensor(Robot.Direction.RIGHT);
                                    rightSensor.setBackgroundColor(Color.GREEN);
                                    driver.triggerUpdateSensorInformation();
                                } else {
                                    Log.v(TAG, "Right sensor failure/repair stopped by button press.");
                                    rightSensor.setBackgroundColor(Color.LTGRAY);
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (rightIsInRepairCycle) {
                    rightSensorThread.start();
                    rightSensor.setBackgroundColor(Color.GREEN);
                } else {
                    rightSensorThread.interrupt();
                }
            }
        });
    }

    // -----------------------------------END ANDROID FOCUSED CODE----------------------------------
    /**
     * Initializes the drawer for the first person view
     * and the map view and then draws the initial screen
     * for this state.
     */
    protected void startDrawer() {
        firstPersonView = new FirstPersonView(Constants.VIEW_WIDTH,
                Constants.VIEW_HEIGHT, Constants.MAP_UNIT,
                Constants.STEP_SIZE, seenCells, maze.getRootnode()) ;
        mapView = new Map(seenCells, 75, maze) ;
        // draw the initial screen for this state
        draw();
    }

    /**
     * Draws the current content on panel to show it on screen.
     */
    protected void draw() {
        if (panel == null) {
            return;
        }

        if (!startedDriver || !paused) {

            // thread is used to change the progress value
            batteryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    progBarHandler.post(new Runnable() {
                        public void run() {
                            batteryVal.setText("Battery: " + (int)robot.getBatteryLevel());
                            batteryProgress.setProgress((int) robot.getBatteryLevel());
                        }
                    });
                }
            });
            batteryThread.start();


            // draw the first person view and the map view if wanted
            firstPersonView.draw(panel, px, py, walkStep, angle);
            if (isInMapMode()) {
                mapView.draw(panel, px, py, angle, walkStep,
                        isInShowMazeMode(), isInShowSolutionMode());
            }
            // update the screen with the buffer graphics
            panel.update();
        } else {
//            while (!interruptedThreads) { // stop the animation while paused-- don't add frames to queue either
                if (paused) {
                    if (!interruptedThreads) {
                        try {
                            Log.v(TAG, "Waiting driver while paused...");
                            driverThread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
//                        break;
                        }
                    }
                }
//            }
        }
    }

    /**
     * Adjusts the internal map scale setting for the map view.
     * @param increment if true increase, otherwise decrease scale for map
     */
    private void adjustMapScale(boolean increment) {
        if (increment) {
            mapView.incrementMapScale() ;
        }
        else {
            mapView.decrementMapScale() ;
        }
    }

    ////////////////////////////// set methods ///////////////////////////////////////////////////////////////
    ////////////////////////////// Actions that can be performed on the maze model ///////////////////////////
    protected void setCurrentPosition(int x, int y) {
        px = x ;
        py = y ;
    }
    private void setCurrentDirection(int x, int y) {
        dx = x ;
        dy = y ;
    }
    /**
     * Sets fields dx and dy to be consistent with
     * current setting of field angle.
     */
    private void setDirectionToMatchCurrentAngle() {
        setCurrentDirection((int) Math.cos(radify(angle)), (int) Math.sin(radify(angle))) ;
    }

    ////////////////////////////// get methods ///////////////////////////////////////////////////////////////
    protected int[] getCurrentPosition() {
        int[] result = new int[2];
        result[0] = px;
        result[1] = py;
        return result;
    }
    protected CardinalDirection getCurrentDirection() {
        return CardinalDirection.getDirection(dx, dy);
    }
    boolean isInMapMode() {
        return mapMode ;
    }
    boolean isInShowMazeMode() {
        return showMaze ;
    }
    boolean isInShowSolutionMode() {
        return showSolution ;
    }
    public Maze getMazeConfiguration() {
        return maze ;
    }
    //////////////////////// Methods for move and rotate operations ///////////////
    final double radify(int x) {
        return x*Math.PI/180;
    }
    /**
     * Helper method for walk()
     * @param dir
     * @return true if there is no wall in this direction
     */
    protected boolean checkMove(int dir) {
        CardinalDirection cd = null;
        switch (dir) {
            case 1: // forward
                cd = getCurrentDirection();
                break;
            case -1: // backward
                cd = getCurrentDirection().oppositeDirection();
                break;
            default:
                throw new RuntimeException("Unexpected direction value: " + dir);
        }
        return !maze.hasWall(px, py, cd);
    }
    /**
     * Pauses thread the driver is running in to smooth the animation.
     */
    private void slowedDownRedraw() {
        draw() ;
        try {
            driverThread.sleep(25); // increased from 25 millis
        } catch (Exception e) {
            // may happen if thread is interrupted
            // no reason to do anything about it, ignore exception
        }
    }

    /**
     * Performs a rotation with 4 intermediate views,
     * updates the screen and the internal direction
     * @param dir for current direction, values are either 1 or -1
     */
    synchronized private void rotate(int dir) {
        final int originalAngle = angle;
        final int steps = 4;

        for (int i = 0; i != steps; i++) {
            // add 1/4 of 90 degrees per step
            // if dir is -1 then subtract instead of addition
            angle = originalAngle + dir*(90*(i+1))/steps;
            angle = (angle+1800) % 360;
            // draw method is called and uses angle field for direction
            // information.
            slowedDownRedraw();
        }
        // update maze direction only after intermediate steps are done
        // because choice of direction values are more limited.
        setDirectionToMatchCurrentAngle();
        //logPosition(); // debugging

    }

    /**
     * Moves in the given direction with 4 intermediate steps,
     * updates the screen and the internal position
     * @param dir, only possible values are 1 (forward) and -1 (backward)
     */
    synchronized private void walk(int dir) {
        // check if there is a wall in the way
        if (!checkMove(dir))
            return;
        // walkStep is a parameter of FirstPersonDrawer.draw()
        // it is used there for scaling steps
        // so walkStep is implicitly used in slowedDownRedraw
        // which triggers the draw operation in
        // FirstPersonDrawer and MapDrawer
        for (int step = 0; step != 4; step++) {
            walkStep += dir;
            slowedDownRedraw();
        }
        setCurrentPosition(px + dir*dx, py + dir*dy) ;
        walkStep = 0; // reset counter for next time
        //logPosition(); // debugging
    }

    /**
     * Checks if the given position is outside the maze
     * @param x coordinate of position
     * @param y coordinate of position
     * @return true if position is outside, false otherwise
     */
    private boolean isOutside(int x, int y) {
        return !maze.isValidPosition(x, y) ;
    }

    /**
     * Internal method to set the current position, the direction
     * and the viewing direction to values consistent with the
     * given maze.
     */
    private void setPositionDirectionViewingDirection() {
        // obtain starting position
        int[] start = maze.getStartingPosition() ;
        setCurrentPosition(start[0],start[1]) ;
        // set current view direction and angle
        angle = 0; // angle matches with east direction,
        // hidden consistency constraint!
        setDirectionToMatchCurrentAngle();
        // initial direction is east, check this for sanity:
        assert(dx == 1);
        assert(dy == 0);
    }

    public boolean keyDown(Constants.UserInput key, int value) {
        switch(key) {
            case Up:
                walk(1);
//                if (isOutside(px, py)) {
//                }
                break;
            case Left:
                rotate(1);
                break;
            case Right:
                rotate(-1);
                break;
            case Down:
                walk(-1);
                break;
            case Jump:
                if (maze.isValidPosition(px + dx, py + dy)) {
                    setCurrentPosition(px + dx, py + dy) ;
                    draw();
                }
        }
        return true;
    }

    /**
     * Initializes robot and driver based on what the user selected
     * at the title screen.
     * @author Phillip Watkins
     */
    private void initializeRobotAndDriver() {
        // Create robot and driver so they can explore the maze
        robot = new BasicRobot();
        Intent accessDriver = getIntent();
        String choiceOfDriver = accessDriver.getStringExtra("driver");
        switch(choiceOfDriver) {
            case "WallFollower":
                driver = new WallFollower();
                break;
            case "Wizard":
                driver = new Wizard();
                break;
        }
        robot.setMaze(this);
        robot.setBatteryLevel(3000);
        robot.resetOdometer();
        driver.setRobot(robot);
        driver.setDistance(getMazeConfiguration().getMazedists());
        shortestPathLength = maze.getDistanceToExit(maze.getStartingPosition()[0], maze.getStartingPosition()[1]);

    }

    /**
     * Passes battery consumption, path length, and shortest path length
     * to Win/Lose activity, then proceeds to that activity.
     * @author Phillip Watkins
     */
    private void giveBatteryAndPathInformation() {
        if (wonGame)
            shareGamedata = new Intent(this, WinningActivity.class);
        else if (!wonGame) {
            shareGamedata = new Intent(this, LosingActivity.class);
            shareGamedata.putExtra("reason", robot.getReason()); // get reason for loss
        }

        shareGamedata.putExtra("battery", driver.getEnergyConsumption());
        shareGamedata.putExtra("pathlen", driver.getPathLength());
        shareGamedata.putExtra("shortest", shortestPathLength);

        // Set maze to null so it will be garbage collected
        ((AMazeGlobalContainer)getApplication()).destroyMaze();

        if (driverThread != null && !driverThread.isInterrupted())
            driverThread.interrupt();

        // Move to win/lose screen
//        if (!paused)
        if (!interruptedThreads)
            startActivity(shareGamedata);


        driver = null;
        robot = null;
        finish();
    }

}

