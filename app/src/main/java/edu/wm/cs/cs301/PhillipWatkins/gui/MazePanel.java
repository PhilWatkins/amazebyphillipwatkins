package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import edu.wm.cs.cs301.PhillipWatkins.R;
import edu.wm.cs.cs301.PhillipWatkins.generation.SingleRandom;

public class MazePanel extends View {

    private Canvas tempCanvas; // similar to awt graphics
    private Bitmap bitmap; // used to draw to UI canvas once all the drawing on tempCanvas is done
    private Paint paint; // mostly for setting color
    private boolean isFloor = false;
    private boolean isCeiling = false;
    private Drawable ceiling;
    private Drawable floor;
    private Rect rect;
    private Bitmap wallTexture;
    private BitmapShader wall;
    private Paint wallPaint;
    private boolean manual;
    private boolean showJumpscares;
    private int randomVal;

    // Image for jumpscare is randomly chosen from the 4 below
    private Bitmap jmpScare1;
    private Bitmap jmpScare2;
    private Bitmap jmpScare3;
    private MediaPlayer playScream;
    /**
     * Auto-generated constructor
     * @param context
     */
    public MazePanel(Context context) {
        super(context);
        init(null);
    }

    // Constructors called because view is in XML
    public MazePanel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MazePanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    /**
     * Initializes the temporary canvas and bitmap used to store drawings.
     * @param set
     */
    private void init(@Nullable AttributeSet set) {
        manual = false;
        showJumpscares = true;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG); // anti_alias_flag to make drawings smoother and less pixelated looking
        paint.setStyle(Paint.Style.FILL); // Will it ever not be fill?
        wallPaint = new Paint((Paint.ANTI_ALIAS_FLAG));
        bitmap = Bitmap.createBitmap(1400, 1400, Bitmap.Config.ARGB_8888);
        tempCanvas = new Canvas(bitmap); // drawing on tempCanvas will update the bitmap. Bitmap can be drawn onto UI canvas with onDraw()
        floor = getResources().getDrawable(R.drawable.texture);
        ceiling = getResources().getDrawable(R.drawable.texture2);
        wallTexture = BitmapFactory.decodeResource(getResources(), R.drawable.redbrick);
        wall = new BitmapShader(wallTexture, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        wallPaint.setShader(wall);
        jmpScare1 = BitmapFactory.decodeResource(getResources(), R.drawable.jumpscarenun);
        jmpScare2 = BitmapFactory.decodeResource(getResources(), R.drawable.jumpscaretwo);
        jmpScare3 = BitmapFactory.decodeResource(getResources(), R.drawable.jumpscarethree);
    }

    @Override
    protected void onDraw(Canvas canvas) { // Canvas is 1400 x 1400
        randomVal = SingleRandom.getRandom().nextIntWithinInterval(0, 100);
        // Shows jump scare based on which random int was generated. 1 show jumpscare 1, 2 shows jumpscare 2... etc.
        if (showJumpscares && randomVal == 1 && manual) {
            canvas.drawBitmap(jmpScare1, 0, 0, null); // draw the jumpscare
            playScream = MediaPlayer.create(getContext(), R.raw.thunder);
            playScream.start();
        } else if (showJumpscares && randomVal == 2 && manual) {
            canvas.drawBitmap(jmpScare2, 0, 0, null); // draw the jumpscare
            playScream = MediaPlayer.create(getContext(), R.raw.thunder);
            playScream.start();
        } else if (showJumpscares && randomVal == 3 && manual) {
            canvas.drawBitmap(jmpScare3, 0, 0, null); // draw the jumpscare
            playScream = MediaPlayer.create(getContext(), R.raw.thunder);
            playScream.start();
        }

        else
            canvas.drawBitmap(bitmap, 0, 0, paint); // draw temp canvas to UI canvas (KEEP THIS LINE)
    }

    /**
     * Method to draw the buffer image on a graphics object that is
     * obtained from the superclass.
     * Warning: do not override getGraphics() or drawing might fail.
     */
    public void update() {
        if (tempCanvas == null) {
            return;
        }
        invalidate(); // calls onDraw() to but bitmap onto canvas. How to put things onto bitmap though?
    }

//    /**
//     * Draws the buffer image to the given graphics object.
//     * This method is called when this panel should redraw itself.
//     * The given graphics object is the one that actually shows
//     * on the screen.
//     */
//    public void paint(Canvas canvas) {
//        if (null == tempCanvas) {
//            System.out.println("MazePanel.paint: no canvas object, skipping drawImage operation");
//        }
//        else {
//            update();
//        }
//    }

    public void setColor(int r, int g, int b) {
        paint.setARGB(255, r, g, b);
    }

    public void fillPolygon(int[] xps, int[] yps, int nps) {
        Path polygon = new Path();
        polygon.moveTo(xps[0], yps[0]); // start at the first x, y

        // move pencil in a straight line to the next point in the polygon
        for (int i=1; i<nps; i++) // skip first point, already moved to it in line above
            polygon.lineTo(xps[i], yps[i]);

        // return to starting point
        polygon.lineTo(xps[0], yps[0]);

        // fill in space between points with the wall texture drawable
        if (manual)
            tempCanvas.drawPath(polygon, wallPaint);
        else
            tempCanvas.drawPath(polygon, paint);
    }

    /**
     * Only used for displaying the floor on the bottom half, and the empty void on the
     * top half of the view. Drawables are only shown in PlayManually. Walls
     * will be solod colored rectangles as before in PlayAnimationActivity.
     * @param x
     * @param y
     * @param w
     * @param h
     */
    public void fillRect(int x, int y, int w, int h) {
        if (manual) { // Drawables are only drawn in Manual mode. The solid-colored walls are drawn in PlayAnimationActivity.
            if (isFloor && !isCeiling) {
                rect = new Rect(x, y, (x + w), (y + h));
                floor.setBounds(rect);
                floor.draw(tempCanvas);
            } else if (!isFloor && isCeiling) {
                rect = new Rect(x, y, (x + w), (y + h));
                ceiling.setBounds(rect);
                ceiling.draw(tempCanvas);
            }
        } else {
            tempCanvas.drawRect(new Rect(x, y, (x + w), (y + h)), paint); // draw plain solid color wall
        }
    }

    public void fillOval(int x, int y, int width, int height) {
        tempCanvas.drawOval(new RectF(x, y, (x + width), (y + height)), paint);
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        paint.setStrokeWidth(2);
        tempCanvas.drawLine(x1, y1, x2, y2, paint);
    }

    /**
     * Method to excercise MazePanel functionality.
     */
//    public void testMazePanel() {
//        setColor(0, 255, 0); // anything green was set by this
//
//        // x, y coords for a 1400 x 1400 polygon. This should fill the entire maze view area
//        int[] xps = { 0, 0, 1400, 1400};
//        int[] yps = { 0, 1400, 1400, 0};
//        fillPolygon(xps, yps, 4);
//
//
//        setColor(125, 125, 125);
//        fillOval(600, 600, 900, 900); // should draw on top of 1400 x 1400 polygon
//
//        // Draw line works, but it's thin and easy to miss
//        setColor(180, 0, 180);
//        drawLine(40, 40, 800, 800);
//
//        fillRect(0, 0, 100, 100); // create 100 by 100 rectangle in top left of view
//        invalidate();
//    }

    /**
     *  Alerts panel that the rect being drawn should use drawable
     *  drawable for the ceiling instead of drawing the black void.
     */
    public void alertToDrawCeiling() {
        isCeiling = true;
        isFloor = false;
    }

    /**
     * Alerts the panel that the rect being drawn should use the
     * drawable for the floor instead of doing a grey rectangle.
     */
    public void alertToDrawFloor() {
        isFloor = true;
        isCeiling = false;
    }

    /**
     * Different graphics are shown when playing manually.
     * The robot driver was laggy with the wall/floor/ceiling images
     * being drawn, so the basic walls will be shown in PlayAnimation, but
     * play manually will show the drawables.
     */
    public void setManual() {
        manual = true;
    }

//    public void showJumpScare() {
//        tempCanvas.drawBitmap(jmpScare1, 0, 0, null);
//    }

    public void releaseSounds() {
        if (playScream != null) {
            playScream.pause();
            playScream.release();
        }
    }
}
