package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.PhillipWatkins.R;
import edu.wm.cs.cs301.PhillipWatkins.generation.AMazeGlobalContainer;
import edu.wm.cs.cs301.PhillipWatkins.generation.CardinalDirection;
import edu.wm.cs.cs301.PhillipWatkins.generation.Floorplan;
import edu.wm.cs.cs301.PhillipWatkins.generation.Maze;

/**
 * Shows an animation of the driver navigating through the maze.
 * Also responsible for recognizing if the player won or lost, and
 * moving to WinningActivity or LosingActivity based on the outcome.
 * @author Phillip Watkins
 */
public class PlayManuallyActivity extends AppCompatActivity {

    private static final String TAG = "PlayManuallyActivity"; // Tag to tell where message came from
    private static final float MOVE_COST = 5;
    private static final float ROTATE_COST = 3;

//    Button playThunderSound; // Testing out playing sounds

    private Button goToWinning; // takes user to winning activity when clicked
    private Button goToLosing; // takes user to losing activity when clicked
    private ImageButton moveUpBtn;
    private ImageButton moveLeftBtn;
    private ImageButton moveRightBtn;
    private Switch toggleMap;
    private Switch toggleShowWalls;
    private Switch toggleShowSolution;
    private int mapZoomLevel = 75; // 0 is smallest zoom, 15 is default
    private Button zoomIn;
    private Button zoomOut;
    private TextView displayZoomValue;
    Intent nextScreen; // used for sharing battery level, path length... via putExtra()
    MazePanel panel;
    Maze maze;
    FirstPersonView firstPersonView;
    Map mapView;
    boolean showMaze, showSolution, mapMode;
    int px, py ; // current position on maze grid (x,y)
    int dx, dy;  // current direction

    int angle; // current viewing angle, east == 0 degrees
    int walkStep; // counter for intermediate steps within a single step forward or backward
    Floorplan seenCells;

    float manualBattery;
    int pathLength;
    int shortestPathLength;
    private MediaPlayer ambientNoises;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually); // view in in the XML for this activity, so it gets set

        manualBattery = 3000;
        panel = findViewById(R.id.playingView);
        panel.setManual();

        // Retreive the Maze-- It will have been set by GeneratingActivity already
        maze = ((AMazeGlobalContainer) getApplication()).getGlobalMaze();
        shortestPathLength = maze.getDistanceToExit(maze.getStartingPosition()[0], maze.getStartingPosition()[1]);

        showMaze = false ;
        showSolution = false ;
        mapMode = false;
        // init data structure for visible walls
        seenCells = new Floorplan(maze.getWidth()+1,maze.getHeight()+1) ;
        // set the current position and direction consistently with the viewing direction
        setPositionDirectionViewingDirection();
        walkStep = 0; // counts incremental steps during move/rotate operation
        if (panel != null) {
            startDrawer();
        }

        // Create movement buttons
        moveUpBtn = findViewById(R.id.upArrowButton);
        moveLeftBtn = findViewById(R.id.leftArrowButton);
        moveRightBtn = findViewById(R.id.rightArrowButton);

        // Set up each button
        setUpForwardButton();
        setUpLeftButton();
        setUpRightButton();

        // Create switches
        toggleMap = findViewById(R.id.mapToggleAnim);
        toggleShowSolution = findViewById(R.id.solutionToggleAnim);
        toggleShowWalls = findViewById(R.id.showWallsToggleAnim);

        // Set up toggle switches for map view
        setUpMapToggle();
        setUpShowSolutionToggle();
        setUpShowWallsToggle();

        // Set up things needed for map's zoom in/out functionality
        zoomIn = findViewById(R.id.mapZoomIn);
        zoomOut = findViewById(R.id.mapZoomOut);
        displayZoomValue = findViewById(R.id.mapZoomValue);
        displayZoomValue.setText("Map zoom level: 75"); // Default zoom is 15/30
        setUpZoomButtons();
        pathLength = 0;

        startAmbientSounds();
    }

    /**
     * Returns to title screen activity instead of previous screen
     */
    @Override
    public void onBackPressed() {
        ambientNoises.pause();
        ambientNoises.release();
        panel.releaseSounds();
        Log.v(TAG, "Stopping ambient sound loop");
        super.onBackPressed();
        startActivity(new Intent(this, AMazeActivity.class));
        finish();
    }


    /**
     * Starts playing a loop of ambient sounds. Gets paused when:
     * 1. The player proceeds to the win/lose screen.
     * 2. The player presses the back button before finishing the game.
     */
    private void startAmbientSounds() {
        ambientNoises = MediaPlayer.create(this, R.raw.ambientnoise);
        ambientNoises.setLooping(true);
        ambientNoises.start();
        Log.v(TAG, "Started ambient sounds");
    }

    /**
     * Creates listener for forward button. Moves player forward 1 step if pressed.
     */
    private void setUpForwardButton() {
        moveUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                walk(1);
                Log.v(TAG, "Move up/forward buton was pressed");
            }
        });

    }

    /**
     * Creates listener for left button. Rotates the player view left when clicked.
     */
    private void setUpLeftButton() {
        moveLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate(1);
                Log.v(TAG, "Move left button was pressed");
            }
        });

    }

    /**
     * Creates listener for right button. Rotates the player view right when clicked.
     */
    private void setUpRightButton() {
        moveRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rotate(-1);
                Log.v(TAG, "Move right button was pressed");
            }
        });

    }

    /**
     * Adds onCheckedChange listener to map toggle
     */
    private void setUpMapToggle() {
        toggleMap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mapMode = !mapMode; // on --> off, off --> on
                draw();
                Log.v(TAG, "Show map view switch toggled.");
            }
        });
    }

    /**
     * Adds listener to show solution toggle switch
     */
    private void setUpShowSolutionToggle() {
        toggleShowSolution.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showSolution = !showSolution;
                draw();
                Log.v(TAG, "Show solution switch toggled.");
            }
        });
    }

    /**
     * Adds listener to show walls toggle switch
     */
    private void setUpShowWallsToggle() {
        toggleShowWalls.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                showMaze = !showMaze;
                draw();
                Log.v(TAG, "Show walls switch toggled.");
            }
        });
    }

    /**
     * Sets up zoom in/out buttons and adjust the textview to display the zoom level (0-30)
     * 0 is smallest map view, 30 is largest.
     */
    private void setUpZoomButtons() {
        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapZoomLevel++;
                Log.v(TAG, "Map zoomed in");
                displayZoomValue.setText("Map zoom level: " + (mapZoomLevel));
                adjustMapScale(true);
                draw();
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapZoomLevel--;
                Log.v(TAG, "Map zoomed out");
                displayZoomValue.setText("Map zoom level: " + (mapZoomLevel));
                adjustMapScale(false);
                draw();
            }
        });
    }

    /**
     * Moves to LosingActivity if the player ran out of energy.
     */
    private void proceedToLosingActivity() {
        ambientNoises.pause();
        ambientNoises.release();
        panel.releaseSounds();
        nextScreen = new Intent(this, LosingActivity.class);
        giveWinLoseInformation();
        startActivity(nextScreen);
        ((AMazeGlobalContainer)getApplication()).destroyMaze();
        finish();
    }

    /**
     * Moves to WinningActivity if the player reached the exit.
     */
    private void proceedToWinningActivity() {

        ambientNoises.pause();
        ambientNoises.release();
        panel.releaseSounds();
        nextScreen = new Intent(this, WinningActivity.class);
        giveWinLoseInformation();
        startActivity(nextScreen);
        ((AMazeGlobalContainer)getApplication()).destroyMaze();
        finish();
    }

    /**
     * Responsible for supplying win/lose screen with information such as:
     * Path length
     * Battery level
     * Shortest possible path
     */
    private void giveWinLoseInformation() {
        nextScreen.putExtra("pathlen", pathLength);
        nextScreen.putExtra("shortest", shortestPathLength);
        nextScreen.putExtra("battery", (3000 - manualBattery)); // 3000-manualBattery = battery consumed
        nextScreen.putExtra("reason", "Player got lost exploring manually");
        Log.v(TAG, "Pathlength: "+pathLength);
        Log.v(TAG, "Shortest path possible: "+shortestPathLength);
        Log.v(TAG, "battery level: "+manualBattery);
        Log.v(TAG, "Stopping ambient sound loop");
    }

    //---------------------------END ANDROID FOCUSED CODE------------------------------------------

    /**
     * Initializes the drawer for the first person view
     * and the map view and then draws the initial screen
     * for this state.
     */
    protected void startDrawer() {
        firstPersonView = new FirstPersonView(Constants.VIEW_WIDTH,
                Constants.VIEW_HEIGHT, Constants.MAP_UNIT,
                Constants.STEP_SIZE, seenCells, maze.getRootnode()) ;
        mapView = new Map(seenCells, 75, maze) ;
        // draw the initial screen for this state
        draw();
    }

    /**
     * Draws the current content on panel to show it on screen.
     */
    protected void draw() {
        if (panel == null) {
            return;
        }
        // draw the first person view and the map view if wanted
        firstPersonView.draw(panel, px, py, walkStep, angle) ;
        if (isInMapMode()) {
            mapView.draw(panel, px, py, angle, walkStep,
                    isInShowMazeMode(),isInShowSolutionMode()) ;
        }
        // update the screen with the buffer graphics
        panel.update() ;
    }

    /**
     * Adjusts the internal map scale setting for the map view.
     * @param increment if true increase, otherwise decrease scale for map
     */
    private void adjustMapScale(boolean increment) {
        if (increment) {
            mapView.incrementMapScale() ;
        }
        else {
            mapView.decrementMapScale() ;
        }
    }

    ////////////////////////////// set methods ///////////////////////////////////////////////////////////////
    ////////////////////////////// Actions that can be performed on the maze model ///////////////////////////
    protected void setCurrentPosition(int x, int y) {
        px = x ;
        py = y ;
    }
    private void setCurrentDirection(int x, int y) {
        dx = x ;
        dy = y ;
    }
    /**
     * Sets fields dx and dy to be consistent with
     * current setting of field angle.
     */
    private void setDirectionToMatchCurrentAngle() {
        setCurrentDirection((int) Math.cos(radify(angle)), (int) Math.sin(radify(angle))) ;
    }

    ////////////////////////////// get methods ///////////////////////////////////////////////////////////////
    protected int[] getCurrentPosition() {
        int[] result = new int[2];
        result[0] = px;
        result[1] = py;
        return result;
    }
    protected CardinalDirection getCurrentDirection() {
        return CardinalDirection.getDirection(dx, dy);
    }
    boolean isInMapMode() {
        return mapMode ;
    }
    boolean isInShowMazeMode() {
        return showMaze ;
    }
    boolean isInShowSolutionMode() {
        return showSolution ;
    }
    public Maze getMazeConfiguration() {
        return maze ;
    }
    //////////////////////// Methods for move and rotate operations ///////////////
    final double radify(int x) {
        return x*Math.PI/180;
    }
    /**
     * Helper method for walk()
     * @param dir
     * @return true if there is no wall in this direction
     */
    protected boolean checkMove(int dir) {
        CardinalDirection cd = null;
        switch (dir) {
            case 1: // forward
                cd = getCurrentDirection();
                break;
            case -1: // backward
                cd = getCurrentDirection().oppositeDirection();
                break;
            default:
                throw new RuntimeException("Unexpected direction value: " + dir);
        }
        return !maze.hasWall(px, py, cd);
    }
    /**
     * DOES NOT WORK FOR ANDROID. This version simply calls draw() without smoothing
     * the players motion.
     */
    private void slowedDownRedraw() {
        draw() ;

//        try {
//            Thread.sleep(25);
//        } catch (Exception e) {
//            // may happen if thread is interrupted
//            // no reason to do anything about it, ignore exception
//        }
    }

    /**
     * Performs a rotation with 4 intermediate views,
     * updates the screen and the internal direction
     * @param dir for current direction, values are either 1 or -1
     */
    synchronized private void rotate(int dir) {
        final int originalAngle = angle;
        final int steps = 4;

        for (int i = 0; i != steps; i++) {
            // add 1/4 of 90 degrees per step
            // if dir is -1 then subtract instead of addition
            angle = originalAngle + dir*(90*(i+1))/steps;
            angle = (angle+1800) % 360;
            // draw method is called and uses angle field for direction
            // information.
            slowedDownRedraw();
        }
        // update maze direction only after intermediate steps are done
        // because choice of direction values are more limited.
        setDirectionToMatchCurrentAngle();
        //logPosition(); // debugging


        // Adjust battery for doing a rotation
        if (manualBattery - ROTATE_COST < 0) {
            Log.v(TAG, "Player ran out of battery during a rotation.");
            proceedToLosingActivity();
        } else {
            manualBattery -= ROTATE_COST;
        }

    }

    /**
     * Moves in the given direction with 4 intermediate steps,
     * updates the screen and the internal position
     * @param dir, only possible values are 1 (forward) and -1 (backward)
     */
    synchronized private void walk(int dir) {
        // check if there is a wall in the way
        if (!checkMove(dir))
            return;
        // walkStep is a parameter of FirstPersonDrawer.draw()
        // it is used there for scaling steps
        // so walkStep is implicitly used in slowedDownRedraw
        // which triggers the draw operation in
        // FirstPersonDrawer and MapDrawer

        for (int step = 0; step != 4; step++) {
            walkStep += dir;
            slowedDownRedraw();
        }
        setCurrentPosition(px + dir*dx, py + dir*dy) ;
        walkStep = 0; // reset counter for next time
        //logPosition(); // debugging


        // Adjust battery for doing a move.
        if (manualBattery - MOVE_COST < 0) {
            Log.v(TAG, "Player ran out of battery during a move operation.");
            proceedToLosingActivity();
        } else {
            manualBattery -= MOVE_COST;
            pathLength++;
        }

        // check if player won the game
        if (isOutside(px,py)) {
            Log.v(TAG, "Recognized the player won.");
            proceedToWinningActivity(); // player made it out of the maze
        }
    }

    /**
     * Checks if the given position is outside the maze
     * @param x coordinate of position
     * @param y coordinate of position
     * @return true if position is outside, false otherwise
     */
    private boolean isOutside(int x, int y) {
        return !maze.isValidPosition(x, y) ;
    }

    /**
     * Internal method to set the current position, the direction
     * and the viewing direction to values consistent with the
     * given maze.
     */
    private void setPositionDirectionViewingDirection() {
        // obtain starting position
        int[] start = maze.getStartingPosition() ;
        setCurrentPosition(start[0],start[1]) ;
        // set current view direction and angle
        angle = 0; // angle matches with east direction,
        // hidden consistency constraint!
        setDirectionToMatchCurrentAngle();
        // initial direction is east, check this for sanity:
        assert(dx == 1);
        assert(dy == 0);
    }
}
