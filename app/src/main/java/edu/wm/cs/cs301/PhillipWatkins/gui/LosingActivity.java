package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.PhillipWatkins.R;


/**
 * Shows a screen informing the player they did not reach the exit, along with
 * their path length, battery consumption, and what the shortest possible path
 * through the maze was.
 * @author Phillip Watkins
 */
public class LosingActivity extends AppCompatActivity {

    private static final String TAG = "LosingActivity"; // Tag to tell where message came from
    private float batteryConsumption;
    private int realPathLen;
    private int shortestPathLen;
    private String failureReason;
    TextView batteryLevel;
    TextView pathLen;
    TextView shortestPath;
    TextView reasonForFailureTxt;
    private MediaPlayer thunder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);

        thunder = MediaPlayer.create(this, R.raw.thunder);
        thunder.start();

        getExtraFromPlaying();

        batteryLevel = findViewById(R.id.batteryLevel);
        pathLen = findViewById(R.id.pathLen);
        shortestPath = findViewById(R.id.shortestPath);
        reasonForFailureTxt = findViewById(R.id.reason);
        batteryLevel.setText("Battery consumed: " + batteryConsumption);
        pathLen.setText("Path length: " + realPathLen);
        shortestPath.setText("Shortest possible path: " + shortestPathLen);
        reasonForFailureTxt.setText(failureReason);

    }

    /**
     * Returns to title screen activity instead of previous screen
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        thunder.release();
        startActivity(new Intent(this, AMazeActivity.class));
        finish();
    }

    private void getExtraFromPlaying() {
        Intent receivedIntent = getIntent();
        batteryConsumption = receivedIntent.getFloatExtra("battery", 0);
        realPathLen = receivedIntent.getIntExtra("pathlen", 0);
        shortestPathLen = receivedIntent.getIntExtra("shortest", 0);
        failureReason = receivedIntent.getStringExtra("reason");
        Log.v(TAG, "Received shortest path:" + shortestPathLen);
        Log.v(TAG, "Received path length: " + realPathLen);
        Log.v(TAG, "Received battery level: " + batteryConsumption);
        Log.v(TAG, failureReason);
    }
}
