package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.PhillipWatkins.R;

/**
 * Acts as the title screen and collects information from the
 * player about the maze they want to play. Responsible for
 * passing this information to GeneratingAcvtivity so it
 * can producde the maze.
 * @author Phillip Watkins
 */
public class AMazeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "AMazeActivity"; // Tag to tell where message came from

    private Button exploreButton; // when clicked, creates a new maze
    private Button revisitButton; // creates maze from file based on menu selections, but with same seed
    private Spinner algorithmSpinner; // for selecting the maze generation algorithm
    private Spinner driverSpinner; // for selecting the robot driver (including manual)
    private SeekBar difficultySeekBar; // For selecting difficulty/skillLevel: 0-15

    private int difficultySelected = 0;
    private String driverSelected = "Manual";
    private String algSelected = "DFS";
    private boolean revisiting = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amaze);

        exploreButton = findViewById(R.id.exploreButton);
        revisitButton = findViewById(R.id.revisitBtn);
        algorithmSpinner = findViewById(R.id.algSpinner);
        driverSpinner = findViewById(R.id.driverSpinner);
        difficultySeekBar = findViewById(R.id.skillLevelSeek);

        // Set up seekBar
        setUpDifficultySeekBar();

        // Set up buttons with listeners
        setUpExploreButton();
        setUpRevisitButton();

        // Set up each Spinner
        setUpDriverSpinner();
        setUpAlgorithmSpinner();

        Log.v(TAG, "onCreate() has completed.");

    }

//  ---------------------------------------------------------------------------------------------
    // INTERFACE METHODS for AdapterView.OnItemSelectedListener (required for Spinners)
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Retreive the string
        String selectedItem = parent.getItemAtPosition(position).toString();
        // Show a toast saying which item was selected
//        Toast.makeText(parent.getContext(), selectedItem, Toast.LENGTH_SHORT).show();
        switch(selectedItem) {
            case "DFS":
                algSelected = "DFS";
                break;
            case "Eller":
                algSelected = "Eller";
                break;
            case "Prim":
                algSelected = "Prim";
                break;
            case "Manual":
                driverSelected = "Manual";
                break;
            case "WallFollower":
                driverSelected = "WallFollower";
                break;
            case "Wizard":
                driverSelected = "Wizard";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
//  ---------------------------------------------------------------------------------------------

    /**
     * Executes the code required to set up the Driver selection Spinner
     */
    private void setUpDriverSpinner() {
        // Add driver spinner adapter
        ArrayAdapter<CharSequence> driverAdapter = ArrayAdapter.createFromResource(this,
                R.array.driverChoices, R.layout.support_simple_spinner_dropdown_item);

        driverAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        driverSpinner.setAdapter(driverAdapter);

        // Add listener
        driverSpinner.setOnItemSelectedListener(this);
        Log.v(TAG, "Driver Spinner has been set up.");
    }

    /**
     * Executes the code required to set up the Algorithm selection Spinner
     */
    private void setUpAlgorithmSpinner() {
        // Add algorithm spinner adapter
        ArrayAdapter<CharSequence> algAdapter = ArrayAdapter.createFromResource(this,
                R.array.algorithmChoices, R.layout.support_simple_spinner_dropdown_item);

        algAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        algorithmSpinner.setAdapter(algAdapter);

        // Add listener
        algorithmSpinner.setOnItemSelectedListener(this);
        Log.v(TAG, "Algorithm Spinner has been set up.");
    }

    /**
     * Executes code needed for the Explore button
     */
    private void setUpExploreButton() {
        // Add listener for explore button
        exploreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revisiting = false;
                // Move to generation screen screen with progress bar
                Log.v(TAG, "Explore button clicked.");
                moveToGenerating(v);
            }
        });
    }


    /**
     * Executes code needed for the Revisit button
     */
    private void setUpRevisitButton() {
        // Add listener for revisiting
        revisitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revisiting = true;
                Log.v(TAG, "Revisit button clicked.");
                moveToGenerating(v);
            }
        });
    }

    /**
     * Sets up skill level selection seekbar w/ listener
     */
    private void setUpDifficultySeekBar() {
        difficultySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                difficultySelected = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Display selected value on seekBar stop
                Log.v(TAG, "Skill level selected");
                Toast.makeText(getApplicationContext(), String.valueOf(difficultySelected), Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * Moves view to the generating screen. Might later be adapted to show different messages
     * depending on whether the maze is being loaded from a file, or being made from scratch.
     * @param v the view object corresponding to the button that was clicked (Revisit or Explore)
     */
    private void moveToGenerating(View v) {
        Log.v(TAG, "Moving to GeneratingActivity");
        Intent moveToGenerating = new Intent(v.getContext(), GeneratingActivity.class);

        moveToGenerating.putExtra("choiceOfDriver", driverSelected);
        moveToGenerating.putExtra("choiceOfAlgorithm", algSelected);
        moveToGenerating.putExtra("choiceOfSkillLevel", difficultySelected);
        moveToGenerating.putExtra("revisiting", revisiting);

        Log.v(TAG, "Chosen alg: " + algSelected);
        Log.v(TAG, "Chosen driver: " + driverSelected);
        Log.v(TAG, "Chosen difficulty/skill level: " + difficultySelected);

        startActivityForResult(moveToGenerating, 0);
    }

}
