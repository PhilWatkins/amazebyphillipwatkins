package edu.wm.cs.cs301.PhillipWatkins.gui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import edu.wm.cs.cs301.PhillipWatkins.R;
import edu.wm.cs.cs301.PhillipWatkins.generation.AMazeGlobalContainer;
import edu.wm.cs.cs301.PhillipWatkins.generation.Maze;
import edu.wm.cs.cs301.PhillipWatkins.generation.MazeFactory;
import edu.wm.cs.cs301.PhillipWatkins.generation.Order;
import edu.wm.cs.cs301.PhillipWatkins.generation.SingleRandom;
import edu.wm.cs.cs301.PhillipWatkins.generation.StubOrder;

/**
 * Responsible for generating the maze and moving to either
 * PlayManuallyActivity or PlayAnimationActivity.
 * @author Phillip Watkins
 */
public class GeneratingActivity extends AppCompatActivity{

    private static final String TAG = "GeneratingActivity"; // Tag to tell where message came from

    private ProgressBar progressBar;
    public int progressValue;
    private TextView displayProgressPercentage;
    private Handler progBarHandler = new Handler();
    private Intent moveToMazeView;
    private String driver;
    private String alg;
    private int skillLevel;
    private int seed;
    Thread generatingThread;

    protected MazeFactory myFac;
    private StubOrder myOrder;
    private Maze finalMaze; // This is the delivered maze for testing
    private Order.Builder builder;

    protected SingleRandom random;
    private boolean revisiting;

    public final static String SAVED_PARAMETERS = "saved_parameters";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);

        getTitleScreenSelections();
        progressBar = findViewById(R.id.progressBarGenerating);
        displayProgressPercentage = findViewById(R.id.progressPercentageGenerating);
        progressValue = 0;
        progressBar.setProgress(progressValue);

        random = SingleRandom.getRandom();
        initializeMaze();

    }


    /**
     * Returns user to title screen upon pressing back button.
     */
    @Override
    public void onBackPressed() {
//        myFac.cancel();
        generatingThread.interrupt(); // stop generation thread if back button is pressed
        super.onBackPressed();
        startActivity(new Intent(this, AMazeActivity.class));
        finish(); // remove from stack
    }

    /**
     * Reads the Extra data passed from title screen.
     * Sets values in this Activity to whatever the user selected.
     */
    private void getTitleScreenSelections() {
        Intent receivedIntent = getIntent();
        // Get values from AMazeActivity
        driver = receivedIntent.getStringExtra("choiceOfDriver");
        alg = receivedIntent.getStringExtra("choiceOfAlgorithm");
        skillLevel = receivedIntent.getIntExtra("choiceOfSkillLevel", 0);
        revisiting = receivedIntent.getBooleanExtra("revisiting", false);

        // Log.v each value
        Log.v(TAG, "Received choice of driver: " + driver);
        Log.v(TAG, "Received choice of algorithm: " + alg);
        Log.v(TAG, "Received choice of difficulty: " + (skillLevel));
        selectBuilder(alg);

    }

    /**
     * Moves from generating screen to playing screen.
     * The playing screen navigated to depends on the
     * selected driver. Manual == true --- > PlayManualActivity
     *                  Manual == false --- > PlayAnimationActivity
     */
    public void moveToPlayingActivity() {
        if ( driver.equals("Manual") )
            moveToMazeView = new Intent(this, PlayManuallyActivity.class);
        else
            moveToMazeView = new Intent(this, PlayAnimationActivity.class);
        Log.v(TAG, "Starting move to playing screen");

        // Give PlayAnimationActivity the driver so it can instantiate driver object of that type
        moveToMazeView.putExtra("driver", driver);

        startActivity(moveToMazeView);
    }

    private void selectBuilder(String alg) {
        switch(alg) {
            case "Prim":
                builder = Order.Builder.Prim;
                break;
            case "Eller":
                builder = Order.Builder.Eller;
                break;
            case "DFS":
                builder = Order.Builder.DFS;
                break;
        }
    }

    private void initializeMaze() {
        generatingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (revisiting) {
                    SharedPreferences getSeed = getSharedPreferences(SAVED_PARAMETERS, MODE_PRIVATE);
                    // Get the seed. Default value is a random seed if none could be found. This happens when revisiting a configuration that wasn't played/stored.
                    seed = getSeed.getInt(alg + skillLevel, random.nextIntWithinInterval(0, Integer.MAX_VALUE-1)); // get seed for string key: ex. "Prim12", "Eller5"
                    Log.v(TAG, "Revisiting with seed " + seed + ", but a random seed will be used if no record could be found.");
                    SingleRandom.setSeed(seed);
                    myFac = new MazeFactory(true); // rebuild maze with the previously stored seed
                } else {
                    seed = random.nextIntWithinInterval(0, Integer.MAX_VALUE-1); // Get the random seed for storing
                    Log.v(TAG, "Newly created seed for the exploration: " + seed);
                    SingleRandom.setSeed(seed);
                    myFac = new MazeFactory(true); // create maze with the random seed we retrieved beforehand as if deterministic
                }
                myOrder = new StubOrder();
                myOrder.difficulty = skillLevel;
                myOrder.setBuildType(builder); // can be either (Prim, Eller, DFS)
                myFac.order(myOrder);

                while (progressValue <= 100) {
                    progBarHandler.post(new Runnable() {
                        public void run() {
                            displayProgressPercentage.setText(progressValue + "% completed");
                        }
                    });
                    try {
                        generatingThread.sleep(10); // Thread.sleep(10)?
                        if (myOrder.percentage < 100 && progressValue < myOrder.percentage) {
                            progressValue = myOrder.percentage;
                            progressBar.setProgress(progressValue);

                        } else if (myOrder.percentage == 100) {
                            moveToPlayingActivity(); // might remove later
                            break; // done generating
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.v(TAG, "Generation thread was paused by back button press.");
                        break; // prevents infinite loop-- either maze finished generating, or it got interrupted and sent here
                    }
                }

                myFac.waitTillDelivered();
                finalMaze = myOrder.getMaze();

                // Set Maze to be accessible anywhere in the App
                ((AMazeGlobalContainer) getApplication()).setGlobalMaze(finalMaze);
                Log.v(TAG, "Set maze from GeneratingActivity");

                int h = finalMaze.getHeight(); // for checking if maze actually got created with the given dims
                Log.v(TAG, "Maze height is...: and was accessed successfully" + h);

                // Save the maze: It will overwrite previous seed with most recent seed for given type
                // A revisited maze will be resaved, and if they tried to revisit a maze that didn't exist, then
                // a random seed will be created and saved as if the explore button were pressed.
                saveMaze();

            }
        });
        generatingThread.start();
    }

    private void saveMaze() {
        SharedPreferences sharedPreferences = getSharedPreferences(SAVED_PARAMETERS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(alg+skillLevel, seed);
        editor.apply();
    }
}
