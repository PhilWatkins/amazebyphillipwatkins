package edu.wm.cs.cs301.PhillipWatkins.gui;

import edu.wm.cs.cs301.PhillipWatkins.generation.CardinalDirection;

/**
 * This class implements the Robot interface for later use with different robot drivers.
 * Collaborators: Controller class, specifically StatePlaying
 * @author Phillip Watkins
 *
 */
public class BasicRobot implements Robot {

    private float batteryLevel;
    private boolean stopped = false; // false until robot has stopped which happens when battery runs out
    protected PlayAnimationActivity controller; // assumed to always be in StatePlaying
    private int distanceTrav = 0; // increases as moves/jumps are done
    private CardinalDirection dirFacing = CardinalDirection.East; // stores the intuitive direction with north/south not reversed

    public final float MOVE_COST = 5;
    public final float DISTANCE_SENSING_COST = 1;
    public final float ROTATE_COST = 3;
    public final float JUMP_COST = 50;

    private boolean frontSensor, backSensor, leftSensor, rightSensor, roomSensor, exitSensor;
    String reason = null;
    boolean isCancelled;

    public BasicRobot() {
        batteryLevel = 3000;
        frontSensor = backSensor = leftSensor = rightSensor = roomSensor = exitSensor = true;
        isCancelled = false;
    }

    @Override
    public int[] getCurrentPosition() throws Exception {
        int[] curPosition = controller.getCurrentPosition(); // remember x<width, y<height

        if ( !(controller.getMazeConfiguration().isValidPosition(curPosition[0], curPosition[1])) ) {
            throw new Exception("Position is out of bounds.");
        }
        return curPosition;
    }

    @Override
    public CardinalDirection getCurrentDirection() {
        return controller.getCurrentDirection(); // return dirFacing?
    }

//    @Override
    public void setMaze(PlayAnimationActivity controller) {
        this.controller = controller; // only need this keyword because they're the same exact name
    }

    @Override
    public float getBatteryLevel() {
        return batteryLevel;
    }

    @Override
    public void setBatteryLevel(float level) {
        batteryLevel = level;
    }

    @Override
    public int getOdometerReading() {
        return distanceTrav;
    }

    @Override
    public void resetOdometer() {
        distanceTrav = 0;
    }

    @Override
    public float getEnergyForFullRotation() {
        return 4*ROTATE_COST;
    }

    @Override
    public float getEnergyForStepForward() {
        return MOVE_COST;
    }

    @Override
    public boolean isAtExit() {
        int[] curPosition = controller.getCurrentPosition();
        if ( controller.getMazeConfiguration().getFloorplan().isExitPosition(curPosition[0], curPosition[1]) )
            return true;
        return false;
    }

    @Override
    public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
        if ( !hasOperationalSensor(direction) )
            throw new UnsupportedOperationException
                    ("There is no operational " + direction + " sensor; the robot can't tell if the exit is visible.");

        checkBattery(DISTANCE_SENSING_COST);
        if ( !stopped )
            updateBatteryLevel(DISTANCE_SENSING_COST);
        if ( findDistanceToWall(direction) == Integer.MAX_VALUE )
            return true;

        return false;
    }

    @Override
    public boolean isInsideRoom() throws UnsupportedOperationException {
        if ( !roomSensor )
            throw new UnsupportedOperationException("This robot doesn't support room sensing.");

        int x = controller.getCurrentPosition()[0];
        int y = controller.getCurrentPosition()[1];
        if ( controller.getMazeConfiguration().getFloorplan().isInRoom(x, y) )
            return true;
        else
            return false;
    }

    @Override
    public boolean hasRoomSensor() {
        return roomSensor;
    }

    @Override
    public boolean hasStopped() {
        return stopped;
    }

    @Override
    public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
        if ( !hasOperationalSensor(direction) )
            throw new UnsupportedOperationException
                    ("Can't determine distance-- there is no " + direction + " sensor.");

        checkBattery(DISTANCE_SENSING_COST);
        updateBatteryLevel(DISTANCE_SENSING_COST);
        return findDistanceToWall(direction);
    }
    /**
     * Translates the given direction into the relative CardinalDirection.
     * @param direction (LEFT, RIGHT, FORWARD, BACKWARD)
     * @return the correct CardinalDirection in Forward/Backward... direction relative to current CardinalDirection
     */
    public CardinalDirection decideWallDirection(Direction direction) {
        switch(direction) {
            case FORWARD:
                return dirFacing;
            case BACKWARD:
                return dirFacing.oppositeDirection();
            case LEFT:
                return dirFacing.oppositeDirection().rotateClockwise();
            case RIGHT:
                return dirFacing.rotateClockwise();
            default:
                System.out.println("Invalid wall direction");
                return null;
        }
    }

    @Override
    public boolean hasOperationalSensor(Direction direction) {
        switch(direction) {
            case FORWARD:
                return frontSensor;
            case BACKWARD:
                return backSensor;
            case LEFT:
                return leftSensor;
            case RIGHT:
                return rightSensor;
            default:
                return false;
        }
    }

    @Override
    public void triggerSensorFailure(Direction direction) {
        switch(direction) {
            case LEFT:
                leftSensor = false;
                break;
            case RIGHT:
                rightSensor = false;
                break;
            case FORWARD:
                frontSensor = false;
                break;
            case BACKWARD:
                backSensor = false;
                break;
        }
    }

    @Override
    public boolean repairFailedSensor(Direction direction) {
        boolean returnValue = true; // will return true by default
        switch(direction) {
            case LEFT:
                if (!leftSensor)
                    returnValue = false;
                leftSensor = true;
                break;
            case RIGHT:
                if (!rightSensor)
                    returnValue = false;
                rightSensor = true;
                break;
            case FORWARD:
                if (!frontSensor)
                    returnValue = false;
                frontSensor = true;
                break;
            case BACKWARD:
                if (!backSensor)
                    returnValue = false;
                backSensor = true;
                break;
            default:
                return false;
        }
        return returnValue;
    }

    @Override
    public void rotate(Turn turn) {
        if (turn == Turn.AROUND)
            checkBattery(2*ROTATE_COST); // make sure there's enough battery for 2 turns
        else
            checkBattery(ROTATE_COST);

        if ( !stopped ) {
            switch(turn) {
                case LEFT: // 90 counter clockwise
                    dirFacing = dirFacing.oppositeDirection().rotateClockwise();
                    updateBatteryLevel(ROTATE_COST);
                    controller.keyDown(Constants.UserInput.Left, 0);
                    break;
                case RIGHT:
                    dirFacing = dirFacing.rotateClockwise();
                    updateBatteryLevel(ROTATE_COST);
                    controller.keyDown(Constants.UserInput.Right, 0);
                    break;
                case AROUND: // 180
                    dirFacing = dirFacing.oppositeDirection();
                    updateBatteryLevel(2 * ROTATE_COST); // cost is equivalent to 2 90deg rotations
                    controller.keyDown(Constants.UserInput.Right, 0);
                    controller.keyDown(Constants.UserInput.Right, 0);
                    break;
            }
        }
    } // end rotate()

    @Override
    public void move(int distance, boolean manual) {
        int distToObstacle = findDistanceToWall(Direction.FORWARD);
        if (distToObstacle == 0 && !manual) {
            stopped = true;
            System.out.println("The robot crashed.");
            if (!isAtExit())
                reason = "The robot crashed while moving forward.";
        }

        while( !stopped && distance > 0 ) {
            checkBattery(MOVE_COST);
            if ( !stopped && distToObstacle > 0 ) {
                controller.keyDown(Constants.UserInput.Up, 0); // actuator
                distanceTrav++;
                updateBatteryLevel(MOVE_COST);
                distance--;
            }
        } // while
    }

    @Override
    public void jump() throws Exception {
        checkBattery(JUMP_COST);
        int x = controller.getCurrentPosition()[0];
        int y = controller.getCurrentPosition()[1];

        if ( !stopped ) {
            switch(dirFacing) {
                case North: // if statement checks if we would be out of bounds after jumping the wall
                    if ( !(controller.getMazeConfiguration().isValidPosition(x, y+1)) )
                        throw new Exception("Can't jump North because thats a border wall.");
                    controller.keyDown(Constants.UserInput.Jump, 0);
                    break;
                case South:
                    if ( !(controller.getMazeConfiguration().isValidPosition(x, y-1)) )
                        throw new Exception("Can't jump South because thats a border wall.");
                    controller.keyDown(Constants.UserInput.Jump, 0);
                    break;
                case West:
                    if ( !(controller.getMazeConfiguration().isValidPosition(x-1, y)) )
                        throw new Exception("Can't jump West because thats a border wall.");
                    controller.keyDown(Constants.UserInput.Jump, 0);
                    break;
                case East:
                    if ( !(controller.getMazeConfiguration().isValidPosition(x+1, y)) )
                        throw new Exception("Can't jump East because thats a border wall.");
                    controller.keyDown(Constants.UserInput.Jump, 0);
                    break;
                default:
                    throw new Exception("Field dirFacing did not have a valid value.");
            }
        }
        updateBatteryLevel(JUMP_COST);
        distanceTrav++;
    }

    /**
     * Checks is the batteryLevel is high enough to continue.
     * Changes stopped field to true if robot doesn't have energy to move.
     */
    private void checkBattery(float cost) {
        if (batteryLevel - cost <= 0) {
            stopped = true;
            reason = "The robot ran out of battery";
        }
    }

    /**
     * Decreases battery level by the given cost constant.
     * Input can be ROTATE_COST, DISTANCE_SENSING_COST, JUMP_COST, or MOVE_COST
     * @param cost
     */
    private void updateBatteryLevel(float cost) {
        batteryLevel -= cost;
    }
    /**
     * Helper method that loops until a wall is found in a given direction.
     * @param direction
     * @return number of cells from robot to a wall in the given direction
     */
    private int findDistanceToWall(Direction direction) {
        CardinalDirection wallDirection = decideWallDirection(direction);
        int x = controller.getCurrentPosition()[0];
        int y = controller.getCurrentPosition()[1];
        int distance = 0;

        if ( wallDirection == CardinalDirection.South || wallDirection == CardinalDirection.North )
            wallDirection = wallDirection.oppositeDirection(); // fixes north/south inconsistency
        while( controller.getMazeConfiguration().getFloorplan().hasNoWall(x, y, wallDirection) && !stopped ) {
            if( controller.getMazeConfiguration().getFloorplan().isExitPosition(x, y) )
                return Integer.MAX_VALUE; // sensing in the given direction led to the exit without hitting a wall

            switch(wallDirection) {
                case North:
                    y--;
                    break;
                case South:
                    y++;
                    break;
                case East:
                    x++;
                    break;
                case West:
                    x--;
                    break;
            }
            distance++;
        }
        return distance;
    }

    public void moveBackward(int distance, boolean manual) {
        int distToObstacle = findDistanceToWall(Direction.BACKWARD); // backward instead of forward
        if ( distToObstacle == 0 && !manual ) {
            stopped = true;
            System.out.println("The robot crashed while moving backwards.");
            if (!isAtExit())
                reason = "The robot crashed while moving backward.";
        }

        while( !stopped && distance > 0 ) {
            checkBattery(MOVE_COST);
            if ( !stopped && distToObstacle > 0 ) {
                controller.keyDown(Constants.UserInput.Down, 0); // down instead of up
                distanceTrav++;
                updateBatteryLevel(MOVE_COST);
                distance--;
            }
        } // while
    }

    /**
     * Provides the reason the robot stopped (if it didn't give up due to sensor failures).
     * It will either crash or run out of battery.
     * If the robot reached the exit, the reason is ignored.
     * @return
     */
    public String getReason() {
        return reason;
    }

    /**
     * Called by WallFollower when the robot is missing a (front/left) and (back/right)
     * sensor. It needs both (front and left) or (back and right) to continue.
     * This method stops the robot, and gives the reason for stopping
     * so it can be displayed on the Losing screen.
     */
    protected void giveUp() {
        stopped = true;
        reason = "The robot gave up due to excessive sensor failures.";
    }
}
